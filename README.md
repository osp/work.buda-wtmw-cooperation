# What's the Matter with Cooperation?

Install of Ethertoff, and a Printparty, to produce documentation during "What's the Matter with Cooperation?", a festival at [Kunstencentrum Buda](http://www.budakortrijk.be) in Kortrijk, Belgium, 28→30 April 2016 

What can we learn about our current society by looking at the increasing production of art works that are made in cooperation with their audience?
