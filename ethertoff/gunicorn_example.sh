#!/bin/bash

NAME="ethertoff"                               # Name of the application
USER=username                                  # the user to run as
GROUP=username                                 # the group to run as
DJANGODIR=/path/to/dir/                        # Django project directory
SOCKFILE=/path/to/dir/gunicorn.sock            # we will communicte using this unix socket
NUM_WORKERS=3                                  # how many worker processes should Gunicorn spawn
DJANGO_SETTINGS_MODULE=relearn.settings        # which settings file should Django use
DJANGO_WSGI_MODULE=relearn.wsgi                # WSGI module name
VENV=/path/to/venv/dir/

echo "Starting $NAME as `whoami`"

# Activate the virtual environment
cd $DJANGODIR
source $VENV/bin/activate
export DJANGO_SETTINGS_MODULE=$DJANGO_SETTINGS_MODULE
export PYTHONPATH=$DJANGODIR:$PYTHONPATH

# Create the run directory if it doesn't exist
RUNDIR=$(dirname $SOCKFILE)
test -d $RUNDIR || mkdir -p $RUNDIR

# Start your Django Unicorn
# Programs meant to be run under supervisor should not daemonize themselves (do not use --daemon)
exec $VENV/bin/gunicorn ${DJANGO_WSGI_MODULE} \
  --name $NAME \
  --workers $NUM_WORKERS \
  --user=$USER --group=$GROUP \
  --bind=unix:$SOCKFILE \
  --log-level=debug \
  --log-file=-
